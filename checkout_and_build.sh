#!/bin/bash

#Get username
#read -p 'Input your username: ' usr_name
#Get root password
#read -sp 'Input Root password ;) : ' root_pwd
#echo ""

#Copy envars?
#read -n1 -p "Copy environment variables? [y,n]: " envcpy
#echo ""

#Reboot after done?
#read -n1 -p "Reboot after done? [y,n]: " reboot
#echo ""

#The script itself
#echo $root_pwd | sudo -S echo && sudo -s <<EOF
#All codes will be executed as sudo
mkdir -p Build
cd Build
git init -q
yes | git clone -b Linux_Build --single-branch ssh://git@altssh.bitbucket.org:443/ekocoil/ripaputki-c.git
yes | cp -rf ~/ripaputki-c/keys/repo_key.pub ~/.ssh/ivadub_repo_key.pub
yes | cp -rf ~/ripaputki-c/keys/repo_key ~/.ssh/ivadub_repo_key
cd ripaputki-c/
git clean -d -x -f
cmake ./ -DCMAKE_BUILD_TYPE="Release"
make
killall BSClient
./BSClient/BSClient

#EOF