#!/bin/bash

#Get username
read -p 'Input your username: ' usr_name
#Get root password
read -sp 'Input Root password ;) : ' root_pwd
echo ""

#Copy envars?
read -n1 -p "Copy environment variables? [y,n]: " envcpy
echo ""

#Reboot after done?
read -n1 -p "Reboot after done? [y,n]: " reboot
echo ""

#The script itself
echo $root_pwd | sudo -S echo && sudo -s <<EOF
#All codes will be executed as sudo

#Full list of commands in order, replace ripalaskin with your user name
apt-get update


#Setup remote x11 display, uncomment next line if needed
#cp /home/$usr_name/.Xauthority /root


#Setup build essential
apt-get -y install build-essential


#CMake
apt-get -y install cmake


#Git
apt-get -y install git


#Envars setup
if [[ $envcpy == "Y" || $envcpy == "y" ]]; then
	#Copy
	echo "Copying rlskn_envars_defaults.sh to /etc/profile.d/"
	yes | cp -rf rlskn_envars_defaults.sh /etc/profile.d/
	#Make executable
	chmod +x /etc/profile.d/rlskn_envars_defaults.sh
else 
	echo "You chose not to copy environment variables"
fi


#reboot if asked 
if [[ $reboot == "Y" || $reboot == "y" ]]; then
	echo "rebooting"
	reboot -f
else 
	echo "You chose not to reboot"
fi
EOF
